package dao;
import domain.Employee;

public interface EmployeeDao {
    void addEmployee(Employee employee);
}

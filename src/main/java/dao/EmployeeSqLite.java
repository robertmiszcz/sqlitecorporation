package dao;



import domain.Employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class EmployeeSqLite implements EmployeeDao {

        Connection connection = null;

    public EmployeeSqLite() {
            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:employees.db");
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
            }
            System.out.println("Opened database successfully");
            createTable();
        }

        public void createTable(){

        String sql = "CREATE TABLE  CORPORATION("
                +"id INTEGER PRIMARY KEY AUTOINCREMENT, "
                +"name TEXT NOT NULL, "
                +"surname TEXT NOT NULL, "
                +"department TEXT NOT NULL, "
                +"salary INTEGER NOT NULL"
                + ")";

            try {
                Statement s = connection.createStatement();
                s.executeUpdate(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("Table created successfully");

        }

    public void addEmployee(Employee employee) {

            int id = employee.getId();
            String name = employee.getName();
            String surname = employee.getSurname();
            String department = employee.getDepartment();
            int salary = employee.getSalary();

            String sql = "INSERT INTO CORPORATION(NAME, SURNAME, DEPARTMENT, SALARY) VALUES("
                    +"'"+name+"', "+ "'"+surname + "', " + "'"+ department + "', " + salary + ")";

        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Added new record");
    }
}




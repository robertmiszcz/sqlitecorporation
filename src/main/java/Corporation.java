import dao.EmployeeDao;
import dao.EmployeeSqLite;
import domain.Employee;

public class Corporation {

    private EmployeeDao employeeDao;

    public EmployeeDao getEmployeeDao() {
        return employeeDao;
    }

    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    public static void main(String[] args) {
        Corporation corporation = new Corporation();
        corporation.setEmployeeDao(new EmployeeSqLite());
        corporation.getEmployeeDao().addEmployee(new Employee("Robert","Sitarski","IT",5000));



    }
}
